/**
 * Models package, contains the object definitions of types being saved to the database.
 */
/**
 * @author S. Solomon Darnell
 *
 */
package models;