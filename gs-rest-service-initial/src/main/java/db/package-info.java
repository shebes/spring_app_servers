/**
 * This package is where I test using Mongo and IBM Cloudant databases.
 */
/**
 * @author S. Solomon Darnell
 *
 */
package db;