package db;

import com.mongodb.Block;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.MongoException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Arrays;

import org.bson.Document;

/**
 * Joka, a main thing to remember about Mongo database interaction is the IP whitelist to your cluster.
 * When the security doesn't mean soo much you should use the online mongoDB.atlas manager to make your
 * cluster accessible from any IP address.
 * 
 * @author S. Solomon Darnell
 *
 */
public class MongoController {
	
	private Logger myLog;
	private MongoCollection<Document> collection;
	private MongoClient mongoClient;
	private MongoClientURI mcURI;
	private MongoDatabase db;
	
	private static final String user = "ssd";
	private static final String password = "dss";
	private static final String src = "mongodb://" + user + ":" + password + "@ssdcluster0-shard-00-00-tsny1.gcp.mongodb.net:27017,ssdcluster0-shard-00-01-tsny1.gcp.mongodb.net:27017,ssdcluster0-shard-00-02-tsny1.gcp.mongodb.net:27017/test?ssl=true&replicaSet=SSDCluster0-shard-0&authSource=admin&retryWrites=true";

	public MongoController () {
		Initialize();
		PrintDocumentsInDB();
		//getCollection().insertOne( CreateTestDocument() );
	}
	
	private void Initialize() {
		String funcName = "[" + new Object(){}.getClass().getName() + "-" + new Object(){}.getClass().getEnclosingMethod().getName() + "]";
		myLog = LoggerFactory.getLogger( new Object(){}.getClass() );
		mcURI = new MongoClientURI(src);
		setClient( funcName, new MongoClient(mcURI) );	
		setDB( getClient().getDatabase("test") );
		setCollection( getDB().getCollection("test") );		
	}
	
	private Document CreateTestDocument() {
		String funcName = "[" + new Object(){}.getClass().getName() + "-" + new Object(){}.getClass().getEnclosingMethod().getName() + "]";
		Document doc;
		doc = new Document("name","MongoDB")
			.append("type", "database")
			.append("count",1)
			.append("versions",Arrays.asList("v3.2","v3.0","v2.6"))
			.append("info", new Document("x", 204).append("y", 101));
		myLog.info( "\n" + funcName + "\t" + doc.toJson() );
		return doc;
	}
	
	private void PrintDocumentsInDB() {
		getCollection().find().forEach( (Block<Document>) doc -> {myLog.info(doc.toJson());});
	}
	
	//****************
	// GETTERS
	//****************
	protected MongoDatabase getDB() { return this.db; }
	protected MongoClient getClient() { return this.mongoClient; }
	protected MongoCollection<Document> getCollection() { return this.collection; }
	//****************
	// SETTERS
	//****************
	private void setDB( MongoDatabase db ) { this.db = db; }
	private void setCollection( MongoCollection<Document> coll) { this.collection = coll; }
	private void setClient( String funcName, MongoClient mongoClient ) {
		try {
			this.mongoClient = mongoClient;
		} catch(MongoException mex) {
			System.out.println( funcName + " problem with creating a mongo client\n" + "\t" + mex.getMessage());
			System.exit(-1);			
		}
	}
	
	/**/
	public static void main(String [] args) {
		MongoController mongodb = new MongoController();
		System.out.println(mongodb.toString());
	}
	/**/
}
