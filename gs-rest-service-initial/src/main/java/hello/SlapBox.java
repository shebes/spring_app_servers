package hello;

public class SlapBox {
	
	private final long id;
	private final String content;
	private final String winner;
	
	public SlapBox(long id, String content, String winner) {
		this.id = id;
		this.content = content;
		this.winner = winner;
	}
	
	public long getId() { return this.id; }
	public String getContent() { return this.content; }
	public String getWinner() { return this.winner; }
}
