package hello;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Value {

	private Long id;
	private String quote;
	
	public Value() {}
	
	/////// GETTERS
	public Long getId() { return this.id; }
	
	public String getQuote() { return this.quote; }
	
	/////// SETTERS
	public void setId(Long id) { this.id = id; }
	public void setQuote(String quote) { this.quote = quote; }
	
	////// Other Methods
	@Override
	public String toString() {
		return "Value{" +
			   "id=" + id +
			   ", quote='" + quote + '\'' +
				'}';
	}
	
	
}
